﻿#region 版权说明
/**************************************************************************
 * 文 件 名：FileHelper
 * 命名空间：CodeGenerator.Core
 * 描　　述：
 * 版 本 号：V1.0.0
 * 作　　者：long
 * 创建时间：2019/5/16 19:05:54
 * CLR 版本：4.0.30319.42000
 * 机器名称：DESKTOP-PHQQ0O3
***************************************************************************	
 * 修 改 人：
 * 时    间：
 * 修改说明：
***************************************************************************
 * Copyright  2018 河南广慧会计服务有限公司 Inc. All Rights Reserved
***************************************************************************/
#endregion
using CodeGenerator.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeGenerator.Core
{
    /// <summary>
    /// 文件操作帮助
    /// </summary>
    public class FileHelper
    {
        public static byte[] StreamToBytes(Stream stream)
        {
            using (stream)
            {
                byte[] bytes = new byte[stream.Length];
                stream.Read(bytes, 0, bytes.Length);
                // 设置当前流的位置为流的开始
                stream.Seek(0, SeekOrigin.Begin);
                return bytes;
            }
        }
        /// <summary>
        /// 保存文件
        /// </summary>
        /// <param name="data">上传的文件信息</param>
        /// <param name="savaFilePath">保存的路径名</param>
        public static void SaveFile(byte[] data, string savaFilePath)
        {
            if (data != null && data.Length > 0)
            {
                string path = savaFilePath.Substring(0, savaFilePath.LastIndexOf("\\"));

                DirectoryInfo Drr = new DirectoryInfo(path);
                if (!Drr.Exists)
                {
                    Drr.Create();
                }
                using (FileStream fs = new FileStream(savaFilePath, FileMode.Create, FileAccess.Write))
                {
                    fs.Write(data, 0, data.Length);
                    fs.Flush();
                }
            }
        }
        /// <summary>
        /// 保存文件
        /// </summary>
        /// <param name="?"></param>
        /// <param name="savaFilePath"></param>
        public static void SaveFile(string data, string savaFilePath)
        {
            SaveFile(data, savaFilePath, Encoding.UTF8);
        }
        /// <summary>
        /// 保存文件
        /// </summary>
        /// <param name="data"></param>
        /// <param name="savaFilePath"></param>
        /// <param name="encoding"></param>
        public static void SaveFile(string data, string savaFilePath, Encoding encoding)
        {
            if (!string.IsNullOrEmpty(data))
            {
                string path = savaFilePath.Substring(0, savaFilePath.LastIndexOf("\\"));
                DirectoryInfo Drr = new DirectoryInfo(path);
                if (!Drr.Exists)
                {
                    Drr.Create();
                }
                using (StreamWriter ostReader = new StreamWriter(savaFilePath, false, encoding))
                {
                    ostReader.Write(data);
                }
            }
        }

        /// <summary>
        /// 文件是否存在
        /// </summary>
        /// <param name="filename">文件名，绝对路径</param>
        /// <returns>是否存在</returns>
        public static bool FileExists(string filename)
        {
            return System.IO.File.Exists(filename);
        }

        /// <summary>
        /// 创建文件
        /// </summary>
        /// <param name="filePach">文件名，绝对路径</param>
        /// <param name="content">文件内容</param>
        static public void WriteFile(string filePath, string content)
        {
            WriteFile(filePath, content, false);
        }
        static public void WriteFile(string filePath, string content, bool addend)
        {
            WriteFile(filePath, content, addend, Encoding.UTF8);
        }
        static public void WriteFile(string filePath, string content, Encoding encode)
        {
            WriteFile(filePath, content, false, encode);
        }
        /// <summary>
        /// 创建文件
        /// </summary>
        /// <param name="filePach">文件名，绝对路径</param>
        /// <param name="content">文件内容</param>
        /// <param name="encode">编码格式</param>
        static public void WriteFile(string filePath, string content, bool addend, Encoding encode)
        {
            filePath = filePath.Replace("\\", "/");
            Directory.CreateDirectory(filePath.Substring(0, filePath.LastIndexOf("/")));
            using (StreamWriter ostReader = new StreamWriter(filePath, addend, encode))
            {
                ostReader.Write(content);
            }
        }

        /// <summary>
        /// 读取文件
        /// </summary>
        /// <param name="filePach"></param>
        /// <returns></returns>
        static public string ReadFile(string filePach)
        {
            return ReadFile(filePach, Encoding.UTF8);
        }
        /// <summary>
        /// 读取文件
        /// </summary>
        /// <param name="filePach"></param>
        /// <param name="encode"></param>
        /// <returns></returns>
        static public string ReadFile(string filePach, Encoding encode)
        {
            string strRtn = string.Empty;
            try
            {
                using (StreamReader ostReader = new StreamReader(filePach, encode))
                {
                    strRtn = ostReader.ReadToEnd();
                }
            }
            catch
            { }
            return strRtn;
        }

        /// <summary>
        /// 删除文件
        /// </summary>
        /// <param name="strFilePath">文件绝对路径</param>
        static public void DeleteFile(string strFilePath)
        {
            if (File.Exists(strFilePath))
            {
                File.Delete(strFilePath);
            }
        }

        /// <summary>
        /// 删除文件夹
        /// </summary>
        /// <param name="folder">文件夹</param>
        static public void DeleteFolder(string folder)
        {
            if (Directory.Exists(folder))
            {
                Directory.Delete(folder, true);
            }
        }
        /// <summary>
        /// 是否为合法扩展名
        /// </summary>
        /// <param name="extend"></param>
        /// <param name="allowFileTypes"></param>
        /// <returns></returns>
        static public bool IsAllowExtend(string extend, string allowFileTypes)
        {
            bool blnReturn = false;
            List<string> olsAllowFileType =allowFileTypes.Split(',').ToList();
            if (olsAllowFileType.Contains(extend))
            {
                blnReturn = true;
            }
            return blnReturn;
        }
        /// <summary>
        /// 获取文件扩展名
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        static public string GetFileExtendName(string filePath)
        {
            return filePath.Substring(filePath.LastIndexOf(".") + 1).ToLower();
        }

        /// <summary>
        /// 获取文件扩展名
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        static public string GetFileSimpleName(string filePath)
        {
            string strFileName = GetFileName(filePath);
            return strFileName.Substring(0, strFileName.LastIndexOf("."));
        }

        /// <summary>
        /// 获取文件名
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static string GetFileName(string filePath)
        {
            filePath = filePath.Replace("\\", "/");
            string[] urlArr = filePath.Split('/');
            return urlArr[urlArr.Length - 1].ToLower();
        }
        /// <summary>
        /// 获取文件的文件夹
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static string GetFileFolder(string filePath)
        {
            filePath = filePath.Replace("\\", "/");
            if (filePath.LastIndexOf("/") > 0)
                return filePath.Substring(0, filePath.LastIndexOf("/")).ToLower();
            else
                return string.Empty;
        }

        /// <summary>
        /// 获取文件夹的大小
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static Decimal GetDirectorySize(string filePath)
        {
            if (!Directory.Exists(filePath))
            {
                return 0;
            }

            DirectoryInfo dirInfo = new DirectoryInfo(filePath);
            Decimal odeFileSize = 0;

            foreach (FileSystemInfo fsInfo in dirInfo.GetFileSystemInfos())
            {
                if (fsInfo.Attributes.ToString().ToLower() == "directory")
                {
                    odeFileSize += GetDirectorySize(fsInfo.FullName);
                }
                else
                {
                    FileInfo fiInfo = new FileInfo(fsInfo.FullName);
                    odeFileSize += fiInfo.Length;
                }
            }
            return odeFileSize;
        }


        public static FileStream GetFileStream(string fileName)
        {
            FileStream fileStream = null;
            if (!string.IsNullOrEmpty(fileName) && File.Exists(fileName))
            {
                fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            }
            return fileStream;
        }


        /// <summary>
        /// 返回文件大小
        /// </summary>
        /// <param name="Size"></param>
        /// <returns></returns>
        public static string FormatFileSize(long Size)
        {
            string result = string.Empty;
            long FactSize = 0;
            FactSize = Size;
            if (FactSize < 1024.00)
                result = FactSize.ToString("F2") + " Byte";
            else if (FactSize >= 1024.00 && FactSize < 1048576)
                result = (FactSize / 1024.00).ToString("F2") + " K";
            else if (FactSize >= 1048576 && FactSize < 1073741824)
                result = (FactSize / 1024.00 / 1024.00).ToString("F2") + " M";
            else if (FactSize >= 1073741824)
                result = (FactSize / 1024.00 / 1024.00 / 1024.00).ToString("F2") + " G";
            return result;
        }
    }
}
