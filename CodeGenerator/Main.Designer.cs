﻿namespace CodeGenerator
{
    partial class Main
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.tbAddress = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnCreate = new System.Windows.Forms.Button();
            this.tbConnection = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tbRecord = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbRepos = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbEntities = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbViewModel = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbService = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbRepository = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbDbFolder = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbProject = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.tbAddress);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.btnCreate);
            this.panel1.Controls.Add(this.tbConnection);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 96);
            this.panel1.TabIndex = 0;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(717, 54);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 7;
            this.button3.Text = "浏览…";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // tbAddress
            // 
            this.tbAddress.Location = new System.Drawing.Point(90, 55);
            this.tbAddress.Name = "tbAddress";
            this.tbAddress.Size = new System.Drawing.Size(620, 21);
            this.tbAddress.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 59);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 12);
            this.label7.TabIndex = 4;
            this.label7.Text = "生成地址：";
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(717, 13);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(75, 23);
            this.btnCreate.TabIndex = 3;
            this.btnCreate.Text = "开始生成";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.button2_Click);
            // 
            // tbConnection
            // 
            this.tbConnection.Location = new System.Drawing.Point(89, 15);
            this.tbConnection.Name = "tbConnection";
            this.tbConnection.Size = new System.Drawing.Size(621, 21);
            this.tbConnection.TabIndex = 1;
            this.tbConnection.Text = "Server=192.168.1.172;Database=doukezhuan;uid=sa;pwd=123456;";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "数据库连接：";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 96);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(800, 354);
            this.panel2.TabIndex = 1;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.tbRecord);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(300, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(500, 354);
            this.panel4.TabIndex = 1;
            // 
            // tbRecord
            // 
            this.tbRecord.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbRecord.Location = new System.Drawing.Point(0, 0);
            this.tbRecord.Multiline = true;
            this.tbRecord.Name = "tbRecord";
            this.tbRecord.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbRecord.Size = new System.Drawing.Size(500, 354);
            this.tbRecord.TabIndex = 0;
            this.tbRecord.WordWrap = false;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.textBox1);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Controls.Add(this.tbRepos);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.tbEntities);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.tbViewModel);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.tbService);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.tbRepository);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.tbDbFolder);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.tbProject);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(300, 354);
            this.panel3.TabIndex = 0;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(119, 316);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(169, 21);
            this.textBox1.TabIndex = 15;
            this.textBox1.Text = "AdminMvc";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 320);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(95, 12);
            this.label10.TabIndex = 14;
            this.label10.Text = "Admin项目名称：";
            // 
            // tbRepos
            // 
            this.tbRepos.Location = new System.Drawing.Point(135, 187);
            this.tbRepos.Name = "tbRepos";
            this.tbRepos.Size = new System.Drawing.Size(155, 21);
            this.tbRepos.TabIndex = 13;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(25, 191);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(101, 12);
            this.label9.TabIndex = 12;
            this.label9.Text = "数据仓储文件名：";
            // 
            // tbEntities
            // 
            this.tbEntities.Location = new System.Drawing.Point(135, 144);
            this.tbEntities.Name = "tbEntities";
            this.tbEntities.Size = new System.Drawing.Size(155, 21);
            this.tbEntities.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(27, 148);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(101, 12);
            this.label8.TabIndex = 10;
            this.label8.Text = "数据模型文件名：";
            // 
            // tbViewModel
            // 
            this.tbViewModel.Location = new System.Drawing.Point(74, 273);
            this.tbViewModel.Name = "tbViewModel";
            this.tbViewModel.Size = new System.Drawing.Size(214, 21);
            this.tbViewModel.TabIndex = 9;
            this.tbViewModel.Text = "ViewModel";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 277);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 12);
            this.label6.TabIndex = 8;
            this.label6.Text = "视图模型：";
            // 
            // tbService
            // 
            this.tbService.Location = new System.Drawing.Point(76, 230);
            this.tbService.Name = "tbService";
            this.tbService.Size = new System.Drawing.Size(214, 21);
            this.tbService.TabIndex = 7;
            this.tbService.Text = "Service";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 234);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 12);
            this.label5.TabIndex = 6;
            this.label5.Text = "服务位置：";
            // 
            // tbRepository
            // 
            this.tbRepository.Location = new System.Drawing.Point(76, 101);
            this.tbRepository.Name = "tbRepository";
            this.tbRepository.Size = new System.Drawing.Size(214, 21);
            this.tbRepository.TabIndex = 5;
            this.tbRepository.Text = "Repository";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 105);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 4;
            this.label4.Text = "仓储位置：";
            // 
            // tbDbFolder
            // 
            this.tbDbFolder.Location = new System.Drawing.Point(76, 58);
            this.tbDbFolder.Name = "tbDbFolder";
            this.tbDbFolder.Size = new System.Drawing.Size(214, 21);
            this.tbDbFolder.TabIndex = 3;
            this.tbDbFolder.Text = "DbModel";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "数据模型：";
            // 
            // tbProject
            // 
            this.tbProject.Location = new System.Drawing.Point(76, 15);
            this.tbProject.Name = "tbProject";
            this.tbProject.Size = new System.Drawing.Size(214, 21);
            this.tbProject.TabIndex = 1;
            this.tbProject.Text = "GuangYi.Test.Core";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "项目名称：";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Main";
            this.Text = "代码生成器v1.0";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.TextBox tbConnection;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox tbRecord;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox tbViewModel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbService;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbRepository;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbDbFolder;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbProject;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox tbAddress;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.TextBox tbRepos;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbEntities;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label10;
    }
}

