﻿using CodeGenerator.Core;
using CodeGenerator.ViewModel;
using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace CodeGenerator
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            GeneratorCofing.FolderProject = tbProject.Text.Trim();
            GeneratorCofing.FolderDbModel = tbDbFolder.Text.Trim();
            GeneratorCofing.FolderRepository = tbRepository.Text.Trim();
            GeneratorCofing.FolderService = tbService.Text.Trim();
            GeneratorCofing.FolderView = tbViewModel.Text.Trim();
            GeneratorCofing.Connection = tbConnection.Text.Trim();
            GeneratorCofing.FileDbContext = tbEntities.Text.Trim();
            GeneratorCofing.FileRepositoryName = tbRepos.Text.Trim();
            GeneratorCofing.FolderAdminMvc = this.textBox1.Text.Trim();
            GeneratorCofing.DataBase = Regex.Match(GeneratorCofing.Connection, @"Database=([\s\S]+?);").Groups[1].ToString();
            btnCreate.Enabled = false;
            btnCreate.Text = "生成中…";
            new GeneratorDataModelHelper() { MForm = this }.Build();

            btnCreate.Enabled = true;
            btnCreate.Text = "开始生成";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (this.folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                if (this.folderBrowserDialog1.SelectedPath.Trim() != "")
                {
                    GeneratorCofing.SavePath = this.folderBrowserDialog1.SelectedPath.Trim();
                    this.tbAddress.Text = GeneratorCofing.SavePath;
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            tbProject.Text = GeneratorCofing.FolderProject;
            tbDbFolder.Text = GeneratorCofing.FolderDbModel;
            tbRepository.Text = GeneratorCofing.FolderRepository;
            tbService.Text = GeneratorCofing.FolderService;
            tbViewModel.Text = GeneratorCofing.FolderView;
            this.tbAddress.Text = GeneratorCofing.SavePath;
            tbEntities.Text = GeneratorCofing.FileDbContext;
            tbRepos.Text = GeneratorCofing.FileRepositoryName;
        }

        #region 设置Record信息
        public void SetCollectInfoScrollToCaret(string info)
        {
            if (tbRecord.InvokeRequired)
            {
                this.Invoke((Action)(() =>
                    SetCollectInfoScrollToCaret(info)
                ));
                return;
            }
            tbRecord.AppendText(info + Environment.NewLine);
            tbRecord.ScrollToCaret();
        }
        #endregion


    }
}
