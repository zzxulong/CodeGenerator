﻿#region 版权说明
/**************************************************************************
 * 文 件 名：${FILE_NAME}
 * 命名空间：${NAME_SPACE}.${REPOSITORY_FOLDER_NAME}
 * 描　　述：
 * 版 本 号：V1.0.0
 * 作　　者：${AUTHOR}
 * 创建时间：${CREATE_DATE}
***************************************************************************	
 * 修 改 人：
 * 时    间：
 * 修改说明：
***************************************************************************
 * ${COMPANY}
***************************************************************************/
#endregion
using ${NAME_SPACE}.${DBFOLDER_NAME};
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace ${NAME_SPACE}.${REPOSITORY_FOLDER_NAME}
{
    public partial class ${DB_CONTEXT_NAME} : DbContext
    {
        public ${DB_CONTEXT_NAME}()
        {
        }

        public ${DB_CONTEXT_NAME}(DbContextOptions<${DB_CONTEXT_NAME}> options) : base(options)
        {
        }

    }
}
