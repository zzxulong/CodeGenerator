﻿#region 版权说明
/**************************************************************************
 * 文 件 名：${FILE_NAME}
 * 命名空间：${NAME_SPACE}.${VIEWMODEL_FOLDER_NAME}.${FILE_NAME}
 * 描　　述：
 * 版 本 号：V1.0.0
 * 作　　者：${AUTHOR}
 * 创建时间：${CREATE_DATE}
***************************************************************************	
 * 修 改 人：
 * 时    间：
 * 修改说明：
***************************************************************************
 * ${COMPANY}
***************************************************************************/
#endregion
using System;
using GuangYi.MvcSDK.ViewModel;
using GuangYi.MvcSDK.WebApi;
using ${NAME_SPACE};
using ${NAME_SPACE}.AuthorizeFilters;
using ${NAME_SPACE}.Service.${FILE_NAME};
using ${NAME_SPACE}.ViewModel.${FILE_NAME};
using Microsoft.AspNetCore.Mvc;

namespace GuangYi.${NAME_SPACE_ADMINMVC}.Controllers
{
    [AdminAuthorize(Roles =GlobalConfig.Role_${FILE_NAME})]
    public class ${FILE_NAME}Controller : Controller
    {
        public I${FILE_NAME}Service ${FILE_NAME}Service { get; set; }
        public IActionResult Index (Query${FILE_NAME}Request request, PageModel page)
        {
            var result = ${FILE_NAME}Service.Query(request, ref page);
            ViewBag.SearchRequest = request;
            ViewBag.Page = page;
            return View(result);
        }
        public ActionResult Create ()
        {
            return View();
        }
        [HttpPost]
        [ActionName("Create")]
        public ActionResult CreateSubmit (${FILE_NAME}ViewModel model)
        {
            if (ModelState.IsValid)
            {
                model = ${FILE_NAME}Service.Add(model);
                return RedirectToAction("Index", "Notice");
            }
            return View(model);
        }
        public ActionResult Update (int? id)
        {
            if (!id.HasValue) throw new AggregateException();
            var model = ${FILE_NAME}Service.Get(id.Value);
            return View(model);
        } 
        [HttpPost]
        [ActionName("Update")] 
        public ActionResult UpdateSubmit (${FILE_NAME}ViewModel model)
        {
            if (ModelState.IsValid)
            {
                model = ${FILE_NAME}Service.Update(model);
                return RedirectToAction("ResetIframe", "Home"); 
            }
            return View(model);
        } 
        public JsonResultApi Delete (int? id)
        {
            var response = new BaseWebApiResponse();
            if (!id.HasValue) throw new ArgumentException();
            ${FILE_NAME}Service.Delete(id.Value);
            return JsonResultApi.Create(response);
        }
    }
}
