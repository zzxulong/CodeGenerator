﻿@{
    Layout = "~/Views/Shared/_LayoutSimple.cshtml";
    ViewBag.Title = "添加";
}
@model ${NAME_SPACE}.ViewModel.${FILE_NAME}.${FILE_NAME}ViewModel

<!-- Page Body -->
<div class="page-body">
    <div class="widget radius-bordered">
        <div class="widget-body">
            @using (Html.BeginForm("Create", "${FILE_NAME}", FormMethod.Post, new { @class = "form-horizontal", @id = "Form" }))
            {
                ${MAIN_CODE}
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <input type="submit" value="确定" class="btn btn-primary btn-lg">
                        <input type="reset" value="取消" class="btn btn-sub btn-lg closeIframe">
                    </div>
                </div>
            }
        </div>
    </div>
</div>
<!-- Page Body -->

@section ScriptSection
{
<script src="@Html.AddFileVersion("/ckeditor4/ckeditor.js")"></script>
<script src="@Html.AddFileVersion("/dist/js/dropzone.js")"></script>
<script>
        $(function () {
            uploadFiles({ handler: '#file_pics', inputName: 'Pic',uploadType:'Attachment' });
        });
    </script>
}
