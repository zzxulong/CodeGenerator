﻿#region 版权说明
/**************************************************************************
 * 文 件 名：${FILE_NAME}
 * 命名空间：${NAME_SPACE}.${DBFOLDER_NAME}
 * 描　　述：
 * 版 本 号：V1.0.0
 * 作　　者：${AUTHOR}
 * 创建时间：${CREATE_DATE}
***************************************************************************	
 * 修 改 人：
 * 时    间：
 * 修改说明：
***************************************************************************
 * ${COMPANY}
***************************************************************************/
#endregion
using System;
using System.Collections.Generic;
using System.Text;

///<summary>
///${FILE_REMARK}
///<summary>
namespace ${NAME_SPACE}.${DBFOLDER_NAME}
{
    public partial class ${FILE_NAME_LOWER}
    {
        ${MAIN_CODE}
    }
}
