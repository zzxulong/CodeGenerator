﻿#region 版权说明
/**************************************************************************
 * 文 件 名：${FILE_NAME}
 * 命名空间：${NAME_SPACE}.${REPOSITORY_FOLDER_NAME}
 * 描　　述：
 * 版 本 号：V1.0.0
 * 作　　者：${AUTHOR}
 * 创建时间：${CREATE_DATE}
***************************************************************************	
 * 修 改 人：
 * 时    间：
 * 修改说明：
***************************************************************************
 * ${COMPANY}
***************************************************************************/
#endregion
using ${NAME_SPACE}.${REPOSITORY_FOLDER_NAME};
using GuangYi.MvcSDK.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ${NAME_SPACE}.${REPOSITORY_FOLDER_NAME}
{
     public interface I${REPOSITORY_BASE_NAME}<TEntity> : IRepository<TEntity> where TEntity : class, new()
    {
        ${DB_CONTEXT_NAME} Get${REPOSITORY_BASE_NAME}DbContext();
    }
}
