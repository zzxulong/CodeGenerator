﻿#region 版权说明
/**************************************************************************
 * 文 件 名：${FILE_NAME}
 * 命名空间：${NAME_SPACE}.${SERVICE_FOLDER_NAME}.${FILE_NAME}
 * 描　　述：
 * 版 本 号：V1.0.0
 * 作　　者：${AUTHOR}
 * 创建时间：${CREATE_DATE}
***************************************************************************	
 * 修 改 人：
 * 时    间：
 * 修改说明：
***************************************************************************
 * ${COMPANY}
***************************************************************************/
#endregion
using AutoMapper;
using GuangYi.Common.Helpers;
using GuangYi.MvcSDK.ViewModel;
using ${NAME_SPACE}.${DBFOLDER_NAME};
using ${NAME_SPACE}.ViewModel.${FILE_NAME};
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

///<summary>
///${FILE_REMARK}
///<summary>
namespace ${NAME_SPACE}.${SERVICE_FOLDER_NAME}.${FILE_NAME}
{
    public interface I${FILE_NAME}Service
    {
       /// <summary>
		/// 添加
		/// </summary>
		/// <param name="model"></param>
		/// <returns></returns>
		${FILE_NAME}ViewModel Add(${FILE_NAME}ViewModel model);

		/// <summary>
		/// 修改
		/// </summary>
		/// <param name="model"></param>
		/// <returns></returns>
		${FILE_NAME}ViewModel Update(${FILE_NAME}ViewModel model);

		/// <summary>
		/// 删除
		/// </summary>
		/// <param name="Id"></param>
		void Delete(object id);

		/// <summary>
		/// 获取详情
		/// </summary>
		/// <param name="Id"></param>
		/// <returns></returns>
		${FILE_NAME}ViewModel Get(object id);

		/// <summary>
		/// 获取列表
		/// </summary>
		/// <param name="model"></param>
		/// <returns></returns>
		List<${FILE_NAME}ViewModel> Query(Query${FILE_NAME}Request model);

		/// <summary>
		/// 获取列表（带分页）
		/// </summary>
		/// <param name="model"></param>
		/// <param name="page"></param>
		/// <returns></returns>
		List<${FILE_NAME}ViewModel> Query (Query${FILE_NAME}Request model, ref PageModel page);
    }
}
