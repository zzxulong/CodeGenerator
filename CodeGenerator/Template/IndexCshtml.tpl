﻿@{
    ViewBag.Title = "管理";
    var searchRequest = ViewBag.SearchRequest as ${NAME_SPACE}.ViewModel.${FILE_NAME}.Query${FILE_NAME}Request;
    
}
@model List<${NAME_SPACE}.ViewModel.${FILE_NAME}.${FILE_NAME}ViewModel>
<!-- Page Body -->
<div class="page-body">
    <div class="widget">
        <div class="widget-body">
            <div class="table-toolbar">
            @using (Html.BeginForm("Index", "${FILE_NAME}", FormMethod.Get, new { @class = "form-inline" }))
            {
                <div class="row">
                    <div class="col-md-9">
                        <div class="form-group">
                                @Html.TextBox("SDate",searchRequest.SDate, "{0:yyyy-MM-dd}", new { @data_date_format = "yyyy-mm-dd", @class = "form-control date-picker", @placeholder = "发布日期开始" })
                         </div>
                        <div class="form-group">
                            @Html.TextBox("EDate", searchRequest.EDate,"{0:yyyy-MM-dd}", new { @data_date_format = "yyyy-mm-dd", @class = "form-control date-picker", @placeholder = "发布日期结束" })
                        </div>   
                        <div class="form-group">
                            @Html.DropDownList("Status", ${NAME_SPACE}.ServiceUtility.GetEnumSelectList<${NAME_SPACE}.GlobalConfig.CommunalStatus>(), "请选择", new { @class = "form-control" })
                        </div>  
                        <div class="form-group">
                            @Html.TextBox("Keyword",searchRequest.Keyword, new { @class = "form-control", @placeholder = "关键词" })
                        </div>

                        <div class="form-group">
                            <input type="submit" value="搜索" class="btn btn-primary">
                        </div>
                    </div>
                    <div class="col-md-3 text-right">
                        <a href="javascript:openIframeWindow({title:'@ViewBag.Title',content:'@Url.Action("Create", "${FILE_NAME}")'})" c class="btn btn-primary"><i class="fa fa-plus"></i>@Html.Raw(string.Format("添加{0}", ViewBag.Title))</a>
                    </div>
                </div>
            }
            </div>
            <table class="table table-bordered table-hover table-striped footable" data-show-toggle="true" data-empty="">
                <thead class="bordered-primary">
                    <tr>
                        <th data-type="number">ID</th>
                        ${INDEX_TITLE}
                        <th data-type="html" data-breakpoints="xs">管理</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach (var item in Model)
                    {
                        <tr>
                            <td>@item.${PRIMARY_KEYNAME</td>
                           ${INDEX_ITEM}
                            <td>
                                <a href="javascript:openIframeWindow({title:'@ViewBag.Title',content:'@Url.Action("Update", "${FILE_NAME}", new { @id = item.${PRIMARY_KEYNAME })'})" class="btn btn-default shiny">编辑</a>
                                <a data-id="@item.${PRIMARY_KEYNAME"  class="btn btn-default shiny delete">删除</a>
                            </td>
                        </tr>
                    }
                </tbody>
            </table>
            @await Html.PartialAsync("_PagerPartial", (GuangYi.MvcSDK.ViewModel.PageModel)ViewBag.Page)
        </div>
    </div>
</div>
<!-- Page Body -->
@section ScriptSection
{
    <script>
        $(function () {
            $('.delete').on("click", function () {
                swalConfirm('@Url.Action("Delete","${FILE_NAME}")', { id: $(this).data('id') });
            });
        });
    </script>
}

