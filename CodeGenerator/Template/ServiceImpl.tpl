﻿#region 版权说明
/**************************************************************************
 * 文 件 名：${FILE_NAME}
 * 命名空间：${NAME_SPACE}.${SERVICE_FOLDER_NAME}.${FILE_NAME}
 * 描　　述：
 * 版 本 号：V1.0.0
 * 作　　者：${AUTHOR}
 * 创建时间：${CREATE_DATE}
***************************************************************************	
 * 修 改 人：
 * 时    间：
 * 修改说明：
***************************************************************************
 * ${COMPANY}
***************************************************************************/
#endregion
using AutoMapper;
using GuangYi.Common.Helpers;
using GuangYi.MvcSDK.ViewModel;
using ${NAME_SPACE}.DbModel;
using ${NAME_SPACE}.Repository;
using ${NAME_SPACE}.ViewModel.${FILE_NAME};
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

///<summary>
///${FILE_REMARK}
///<summary>
namespace ${NAME_SPACE}.${SERVICE_FOLDER_NAME}.${FILE_NAME}
{
    public class ${FILE_NAME}ServiceImpl :I${FILE_NAME}Service
	{  
		#region 属性注入
		/// <summary>
		/// 数据仓储
		/// </summary>
        public I${REPOSITORY_BASE_NAME}<${DB_MODEL_Folder}.${FILE_NAME}> ${FILE_NAME}Repository { get; set; }
		#endregion

		#region 辅助方法

		/// <summary>
		/// AutoMaper配置
		/// </summary>
		private static MapperConfiguration AutoMaperConfig = new MapperConfiguration(cfg =>
		{
			//cfg.CreateMap<${FILE_NAME}ViewModel, ${FILE_NAME}>().ForMember(dest => dest.AddDate, opt => opt.Ignore());
			//cfg.CreateMap<${FILE_NAME}, ${FILE_NAME}ViewModel>().ForMember(dest => dest.StatusCName, opt => opt.MapFrom(e => EnumHelper.GetEnumDesc((GlobalConfig.BlockStatus)e.Status)));
			cfg.CreateMap<${FILE_NAME}ViewModel, ${DB_MODEL_Folder}.${FILE_NAME}>();
			cfg.CreateMap<${DB_MODEL_Folder}.${FILE_NAME}, ${FILE_NAME}ViewModel>();            
		});

		/// <summary>
		/// AutoMapper创建
		/// </summary>
		private static IMapper AutoMapper = AutoMaperConfig.CreateMapper();

		/// <summary>
		/// 转换为数据模型
		/// </summary>
		/// <param name="model"></param>
		/// <returns></returns>
		private ${DB_MODEL_Folder}.${FILE_NAME} ConvertToDbModel(${FILE_NAME}ViewModel source)
		{
			return AutoMapper.Map<${DB_MODEL_Folder}.${FILE_NAME}>(source);
		}

		/// <summary>
		/// 根据视图模型赋值
		/// </summary>
		/// <param name="model"></param>
		/// <param name="result"></param>
		private void CloneDbModel(${FILE_NAME}ViewModel source, ${DB_MODEL_Folder}.${FILE_NAME} result)
		{
			AutoMapper.Map(source, result);
		}

		/// <summary>
		/// 转换为视图模型
		/// </summary>
		/// <param name="model"></param>
		/// <returns></returns>
		private ${FILE_NAME}ViewModel ConvertToViewModel(${DB_MODEL_Folder}.${FILE_NAME} source)
		{
			if(source == null) return null;
			var result = new ${FILE_NAME}ViewModel();
			this.CloneViewModel(source, result);
			return result; 
		}

		/// <summary>
		/// 根据数据模型赋值
		/// </summary>
		/// <param name="source"></param>
		/// <returns></returns>
		private void CloneViewModel(${DB_MODEL_Folder}.${FILE_NAME} source,${FILE_NAME}ViewModel dest)
		{
			AutoMapper.Map(source, dest);
		}     
		#endregion

		#region 服务
		/// <summary>
		/// 添加
		/// </summary>
		/// <param name="model"></param>
		/// <returns></returns>
		public ${FILE_NAME}ViewModel Add(${FILE_NAME}ViewModel model)
		{
			var dbEntity = this.ConvertToDbModel(model);
			//dbEntity.AddDate = DateTime.Now;
			${FILE_NAME}Repository.Add(dbEntity);
			return this.ConvertToViewModel(dbEntity);
		}

		/// <summary>
		/// 修改
		/// </summary>
		/// <param name="model"></param>
		/// <returns></returns>
		public ${FILE_NAME}ViewModel Update(${FILE_NAME}ViewModel model)
		{
			var dbEntity = ${FILE_NAME}Repository.Get(model.${PRIMARY_KEYNAME});
			//this.CloneDbModel(model, dbEntity);
${MAIN_CODE}
			${FILE_NAME}Repository.Update(dbEntity);
			return this.ConvertToViewModel(dbEntity);
		}

		/// <summary>
		/// 删除
		/// </summary>
		/// <param name="Id"></param>
		public void Delete(object id)
		{
			${FILE_NAME}Repository.Delete(id);
		}

		/// <summary>
		/// 获取详情
		/// </summary>
		/// <param name="Id"></param>
		/// <returns></returns>
		public ${FILE_NAME}ViewModel Get(object id)
		{
			return this.ConvertToViewModel(${FILE_NAME}Repository.Get(id));
		}

		/// <summary>
		/// 获取列表
		/// </summary>
		/// <param name="model"></param>
		/// <returns></returns>
		public List<${FILE_NAME}ViewModel> Query(Query${FILE_NAME}Request model)
		{
			return ${FILE_NAME}Repository.Query(this.BuildSearchQuery(model), e => e.${PRIMARY_KEYNAME}, SortType.Desc).ToList().ConvertAll(e => this.ConvertToViewModel(e));
		}
		/// <summary>
		/// 获取列表（带分页）
		/// </summary>
		/// <param name="model"></param>
		/// <param name="page"></param>
		/// <returns></returns>
		public List<${FILE_NAME}ViewModel> Query (Query${FILE_NAME}Request model, ref PageModel page)
		{
			return ${FILE_NAME}Repository.Query(this.BuildSearchQuery(model), e => e.${PRIMARY_KEYNAME}, SortType.Desc,ref page).ToList().ConvertAll(e => this.ConvertToViewModel(e));
		}
		#endregion

		#region	private action
		public Expression<Func<DbModel.${FILE_NAME}, bool>> BuildSearchQuery (Query${FILE_NAME}Request model)
		{
			Expression<Func<DbModel.${FILE_NAME}, bool>> predicate=e=>true;
			//if (model.UserId.HasValue)
			//{
			//	predicate = predicate.And(e => e.UserId == model.UserId.Value);
			//}
			return predicate;
		}
		#endregion
 
    }
}
