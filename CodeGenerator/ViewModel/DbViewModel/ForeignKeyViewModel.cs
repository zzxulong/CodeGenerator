﻿#region 版权说明
/**************************************************************************
 * 文 件 名：ForeignKeyViewModel
 * 命名空间：CodeGenerator.ViewModel.DbViewModel
 * 描　　述：
 * 版 本 号：V1.0.0
 * 作　　者：long
 * 创建时间：2019/5/16 18:14:15
 * CLR 版本：4.0.30319.42000
 * 机器名称：DESKTOP-PHQQ0O3
***************************************************************************	
 * 修 改 人：
 * 时    间：
 * 修改说明：
***************************************************************************
 * Copyright  2018 河南广慧会计服务有限公司 Inc. All Rights Reserved
***************************************************************************/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeGenerator.ViewModel.DbViewModel
{
    public class ForeignKeyViewModel
    {
        public string PrimaryKeyTableName { get; set; }
        public string PrimaryKeyName { get; set; }
        public string ForeignKeyTableName { get; set; }
        public string ForeignKeyName { get; set; }
        public string ForeignPrimaryKeyName { get; set; }
    }
}
