﻿#region 版权说明
/**************************************************************************
 * 文 件 名：GeneratorCofing
 * 命名空间：CodeGenerator.ViewModel
 * 描　　述：
 * 版 本 号：V1.0.0
 * 作　　者：long
 * 创建时间：2019/5/16 18:45:51
 * CLR 版本：4.0.30319.42000
 * 机器名称：DESKTOP-PHQQ0O3
***************************************************************************	
 * 修 改 人：
 * 时    间：
 * 修改说明：
***************************************************************************
 * Copyright  2018 河南广慧会计服务有限公司 Inc. All Rights Reserved
***************************************************************************/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeGenerator.ViewModel
{
    public class GeneratorCofing
    {
        public static string Connection = null;
        public static string DataBase = null;

        public static string FileDbContext = "GuangYiDbContext";
        public static string FileRepositoryName = "GuangYiRepository";

        #region 目录地址

        public static string FolderProject = "GuangYi.Core";
        public static string FolderDbModel = "DbModel";
        public static string FolderRepository = "Repository";
        public static string FolderService = "Service";
        public static string FolderView = "ViewModel";
        public static string FolderAdminMvc="AdminMvc";
        public static string SavePath = System.AppDomain.CurrentDomain.BaseDirectory;
        #endregion

        #region 模版
        public static string TplDbModel = "DbModel.tpl";
        public static string TplDbEntities = "BaseEntities.tpl";
        public static string TplIRepository = "IGuangYiRepository.tpl";
        public static string TplRepositoryImpl = "GuangYiRepositoryImpl.tpl";
        public static string TplViewModel = "ViewModel.tpl";
        public static string TplQueryModel = "QueryModel.tpl";
        public static string TplIService = "IService.tpl";
        public static string TplServiceImpl = "ServiceImpl.tpl";
        public static string TplController="Controller.tpl";
        public static string TplIndexCshtml="IndexCshtml.tpl";
        public static string TplCreateCshtml="CreateCshtml.tpl";
        public static string TplUpdateCshtml="UpdateCshtml.tpl";
        #endregion

    }
}
