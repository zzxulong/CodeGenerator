﻿#region 版权说明
/**************************************************************************
 * 文 件 名：SystemFileInfo
 * 命名空间：CodeGenerator.ViewModel
 * 描　　述：
 * 版 本 号：V1.0.0
 * 作　　者：long
 * 创建时间：2019/5/16 19:06:47
 * CLR 版本：4.0.30319.42000
 * 机器名称：DESKTOP-PHQQ0O3
***************************************************************************	
 * 修 改 人：
 * 时    间：
 * 修改说明：
***************************************************************************
 * Copyright  2018 河南广慧会计服务有限公司 Inc. All Rights Reserved
***************************************************************************/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeGenerator.ViewModel
{
    public class SystemFileInfo
    {
        /// <summary>
        /// 文件名称
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 文件名称
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 文件全名（包含路径）
        /// </summary>
        public string FileFullName { get; set; }

        /// <summary>
        /// 文件扩展名
        /// </summary>
        public string FileExtent { get; set; }

        /// <summary>
        /// 是否为文件夹
        /// </summary>
        public bool IsFolder { get; set; }
    }
}
